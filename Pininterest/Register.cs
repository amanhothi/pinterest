﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;


namespace Pininterest
{
    [Activity(Label = "Register")]
    public class Register : Activity
    {
        EditText nameTxt;
        EditText emailidTxt;
        EditText pwdTxt;
        EditText ageTxt;
        Button ctnbtn;
        Android.App.AlertDialog.Builder alert;
        DBHelperClass myDB;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            System.Console.WriteLine("1111111111111111111111111111111");
            myDB = new DBHelperClass(this);


            // Create your application here

            SetContentView(Resource.Layout.registerpg);
            nameTxt = FindViewById<EditText>(Resource.Id.username);
            emailidTxt = FindViewById<EditText>(Resource.Id.emailid);
            pwdTxt = FindViewById<EditText>(Resource.Id.pass);
            ageTxt = FindViewById<EditText>(Resource.Id.age);

            ctnbtn = FindViewById<Button>(Resource.Id.ctnbtn);
            alert = new Android.App.AlertDialog.Builder(this);
            ctnbtn.Click += delegate
             {
                 if (!nameTxt.Equals(""))
                 {
                     string name = nameTxt.Text;
                     string emailid = emailidTxt.Text;
                     string pwd = pwdTxt.Text;
                     int age = int.Parse(ageTxt.Text.ToString());

                     myDB.insertValue(1, name, emailid, pwd, age);
                     System.Console.WriteLine("22222222222222222222222222222222222");
                     Intent login = new Intent(this, typeof(MainActivity));
                     StartActivity(login);

                 }
                 else
                 {
                     alert.SetTitle("Error");
                     alert.SetMessage("please enter the valid data");
                     alert.SetPositiveButton("OK", alertOK);
                     alert.SetNegativeButton("Cancel", alertcancel);
                     Dialog myDialog = alert.Create();
                     myDialog.Show();
                     System.Console.WriteLine("3333333333333333333333333333333333333");
                     
                     System.Console.WriteLine("444444444444444444444444444444444444444");
                 }
             };
        }
        public void alertOK(object sender, Android.Content.DialogClickEventArgs e)
        {

            System.Console.WriteLine("OK Button Pressed");
        }
        public void alertcancel(object sender, Android.Content.DialogClickEventArgs e)
        {

            System.Console.WriteLine("Cancel Button Pressed");
        }
    }
}





       


           

           