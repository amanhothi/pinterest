﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;

namespace Pininterest
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity

    {
        EditText UserName;
        EditText password;
        Button Loginbtn;
        Button Signin;

        DBHelperClass myDB;

        Android.App.AlertDialog.Builder alert;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            myDB = new DBHelperClass(this);
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            UserName = FindViewById<EditText>(Resource.Id.userid);
            password = FindViewById<EditText>(Resource.Id.password);
            Loginbtn = FindViewById<Button>(Resource.Id.loginbtn);
            Signin = FindViewById<Button>(Resource.Id.signup);
            alert = new Android.App.AlertDialog.Builder(this);

            Loginbtn.Click += delegate
            {

                string username_val = UserName.Text;
                string pass_val = password.Text;
                System.Console.WriteLine("Entered Username is : " + username_val);
                System.Console.WriteLine("Entered password is :  " + pass_val);
                if (username_val.Trim().Equals("") || username_val.Length < 0)
                {
                    alert.SetTitle("Error");
                    alert.SetMessage("please enter the valid data");
                    alert.SetPositiveButton("OK", alertOK);
                    alert.SetNegativeButton("Cancel", alertcancel);
                    Dialog myDialog = alert.Create();
                    myDialog.Show();

                }

               
            };
            Signin.Click += delegate

                 {
                     Intent register = new Intent(this, typeof(Register));
                     StartActivity(register);
                 };


        }
        public void alertOK(object sender, Android.Content.DialogClickEventArgs e)
        {

            System.Console.WriteLine("OK Button Pressed");
        }
        public void alertcancel(object sender, Android.Content.DialogClickEventArgs e)
        {

            System.Console.WriteLine("Cancel Button Pressed");
        }
    }
}
